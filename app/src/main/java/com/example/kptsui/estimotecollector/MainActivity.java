package com.example.kptsui.estimotecollector;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Nearable;
import com.estimote.sdk.Region;
import com.estimote.sdk.SystemRequirementsChecker;
import com.estimote.sdk.eddystone.Eddystone;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    public final static String TAG = "MainActivity";
    public final static String BEACON_UUID = "B9407F30-F5F8-466E-AFF9-25556B57FE6D";

    App app;
    EditText inputX, inputY, inputFileName, inputBeaconSize, inputSampleAmount;
    Button btnRecord;
    TextView indicator;
    Handler mHandler;

    int counter = 0;

    ArrayList<Data> list;
    boolean isRecording = false;
    String fileName;
    double x, y;
    int collectTimes;

    private BeaconManager beaconManager;
    private Region region;
    private String scanId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mHandler = new Handler();
        list = new ArrayList<>();
        btnRecord = (Button) findViewById(R.id.btnRecord);
        indicator = (TextView) findViewById(R.id.indicator);

        inputBeaconSize = (EditText) findViewById(R.id.inputBeaconSize);
        inputX = (EditText) findViewById(R.id.inputX);
        inputY = (EditText) findViewById(R.id.inputY);
        inputFileName = (EditText) findViewById(R.id.inputFileName);
        inputSampleAmount = (EditText) findViewById(R.id.inputSampleAmount);
        app = App.getInstance();

        beaconManager = new BeaconManager(this);
        // Should be invoked in #onCreate.
        /*beaconManager.setEddystoneListener(new BeaconManager.EddystoneListener() {
            @Override public void onEddystonesFound(List<Eddystone> eddystones) {
                Log.d(TAG, "Nearby Eddystone beacons: " + eddystones);
                for (Eddystone b : eddystones){
                    Log.d(TAG, "rssi: " + b.rssi + "\n" + b.url);
                }
            }
        });*/

        region = new Region("ranged region",
                UUID.fromString(BEACON_UUID), null, null);

        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, final List<Beacon> beacons) {
                if(!isRecording){
                    Log.d(TAG, "!isRecording");
                    return;
                }

                Log.i(TAG, "Update Beacon, Counter: " + counter);

                // pure List is not modifiable
                final List<Beacon> sortedBeacons = new ArrayList<Beacon>(beacons);
                Collections.sort(sortedBeacons, new Comparator<Beacon>() {
                    @Override
                    public int compare(Beacon b1, Beacon b2) {
                        return b1.getMajor() - b2.getMajor();
                    }
                });

                StringBuilder beaconOrder = new StringBuilder();
                for(Beacon b : sortedBeacons){
                    beaconOrder.append(b.getMajor()).append(", ");
                }
                Log.i(TAG, "Receive Beacons, order: " + beaconOrder.toString());

                if(counter >= collectTimes){ // stop recording
                    counter = 0;
                    isRecording = false;
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            btnRecord.setText("Start Record");
                            Toast.makeText(app, "Record finished", Toast.LENGTH_LONG).show();

                            writeCSV(null);
                            indicator.setText("0");
                        }
                    });
                    return;
                }
                else{
                    counter++;
                }
                try {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {

                            String display = String.valueOf(counter);

                            list.add(new Data(MainActivity.this.x, MainActivity.this.y, sortedBeacons));
                            for(Beacon b : sortedBeacons)
                                display += "\nID: " + b.getMajor() + ", Rssi: " + b.getRssi();

                            indicator.setText(display);
                        }
                    });

                } catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(app, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        // Should be invoked in #onStart.
        /*beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override public void onServiceReady() {
                scanId = beaconManager.startEddystoneScanning();
            }
        });*/

        SystemRequirementsChecker.checkWithDefaultDialogs(this);

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.startRanging(region);
            }
        });
    }

    @Override
    protected void onStop() {
        beaconManager.stopRanging(region);
        super.onStop();

        // Should be invoked in #onStop.
        //beaconManager.stopEddystoneScanning(scanId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // When no longer needed. Should be invoked in #onDestroy.
        beaconManager.disconnect();
    }


    public void startRecord(View v){
        if(inputFileName.getText().toString().isEmpty()
                || inputX.getText().toString().isEmpty()
                || inputY.getText().toString().isEmpty()
                || inputBeaconSize.getText().toString().isEmpty()
                || inputSampleAmount.getText().toString().isEmpty()){
            Toast.makeText(app, "Input all fields!!", Toast.LENGTH_LONG).show();
            return;
        }
        fileName = inputFileName.getText().toString();
        x = Double.parseDouble(inputX.getText().toString());
        y = Double.parseDouble(inputY.getText().toString());
        Data.DEPLOY_BEACONS = Integer.parseInt(inputBeaconSize.getText().toString());
        collectTimes = Integer.parseInt(inputSampleAmount.getText().toString());

        isRecording = true;
        btnRecord.setText("Recording....");
    }

    public void writeCSV(View v){
        if(fileName.isEmpty()){
            return;
        }

        String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/CSV";
        File folder = new File(dir);
        folder.mkdirs();

        File file = new File(folder, fileName + ".csv");

        Log.i(TAG, "saving file: " + fileName + ".csv");
        try {
            FileOutputStream out = new FileOutputStream(file, true); // append

            // write header
            if(file.length() <= 0){
                out.write("x,y".getBytes());
                for(int i = 0; i < Data.DEPLOY_BEACONS; i++){
                    out.write((",beacon"+(i+1)).getBytes());
                }
                out.write("\n".getBytes());
            }

            for (Data data : list){
                out.write(data.toString().getBytes());
            }
            out.close();
            Log.i(TAG, "FileOutputStream close");

            // saving the image file
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScanIntent.setData(Uri.fromFile(file));
            MainActivity.this.sendBroadcast(mediaScanIntent);

            Toast.makeText(app, "Saved", Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Save file failed");
        }

        writeJSONData(list, "");

        ArrayList<Data> avg = averaging(list);
        writeAverageData(avg);
        writeJSONData(avg, "average_");

        list = new ArrayList<>();
    }

    public ArrayList<Data> averaging(ArrayList<Data> list){
        if(list.size() == 1)
            return list;

        ArrayList<Data> averageList = new ArrayList<>();

        if(list == null) return averageList;

        ArrayList<Data> tempList = new ArrayList<>();
        Data lastData = list.get(0);

        for (Data data : list){
            if(lastData.x != data.x || lastData.y != data.y || list.indexOf(data) == list.size() - 1){

                double[] avg_rssi = new double[Data.DEPLOY_BEACONS];
                for(Data d : tempList){
                    for(int i = 0; i < avg_rssi.length; i++) {
                        if (d.beacons[i] != null) {
                            avg_rssi[i] += d.beacons[i].getRssi();
                        }
                    }
                }
                for(int i = 0; i < avg_rssi.length; i++)
                    avg_rssi[i] /= tempList.size();


                lastData.setAverageBeacons(avg_rssi);
                averageList.add(lastData);

                tempList.clear();
            }
            lastData = data;
            tempList.add(data);
        }

        return averageList;
    }

    public void writeAverageData(ArrayList<Data> averageList){
        if(fileName.isEmpty()){
            return;
        }

        String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/CSV";
        File folder = new File(dir);
        folder.mkdirs();

        File file = new File(folder, "average_" + fileName + ".csv");

        Log.i(TAG, "saving file: " + "average_" + fileName + ".csv");
        try {
            FileOutputStream out = new FileOutputStream(file, true); // append

            // write header
            if(file.length() <= 0){
                out.write("x,y".getBytes());
                for(int i = 0; i < Data.DEPLOY_BEACONS; i++){
                    out.write((",beacon"+(i+1)).getBytes());
                }
                out.write("\n".getBytes());
            }

            for (Data data : averageList){
                out.write(data.toString().getBytes());
            }
            out.close();
            Log.i(TAG, "FileOutputStream close");

            // saving the image file
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScanIntent.setData(Uri.fromFile(file));
            MainActivity.this.sendBroadcast(mediaScanIntent);

            Toast.makeText(app, "Saved", Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Save file failed");
        }
    }

    public void writeJSONData(ArrayList<Data> list, String filePrefixName){
        if(fileName.isEmpty()){
            return;
        }

        String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/CSV";
        File folder = new File(dir);
        folder.mkdirs();

        File file = new File(folder, filePrefixName + fileName + ".json");

        Log.i(TAG, "saving file: " + filePrefixName + fileName + ".json");
        try {
            FileOutputStream out = new FileOutputStream(file, true); // not append
            if(file.length() > 0)
                out.write(",".getBytes());

            for (Data data : list){
                JSONObject o = new JSONObject();
                o.put("x", data.x);
                o.put("y", data.y);

                JSONArray beacons = new JSONArray();
                for(Beacon b : data.beacons){
                    JSONObject beacon = new JSONObject();
                    beacon.put("bid", b.getMajor());
                    beacon.put("rssi", b.getRssi());
                    beacons.put(beacon);
                }
                o.put("beacons", beacons);

                out.write(o.toString().getBytes());

                if(list.indexOf(data) + 1 != list.size()){
                    out.write(",".getBytes());
                }
            }

            out.close();
            Log.i(TAG, "FileOutputStream close");

            // saving the image file
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScanIntent.setData(Uri.fromFile(file));
            MainActivity.this.sendBroadcast(mediaScanIntent);

            Toast.makeText(app, "Saved", Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Save file failed");
        }
    }
}
