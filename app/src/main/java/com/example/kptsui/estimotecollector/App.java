package com.example.kptsui.estimotecollector;

import android.app.Application;

import com.estimote.sdk.BeaconManager;

/**
 * Created by kptsui on 27/10/2016.
 */

public class App extends Application {
    public final static String TAG = "Application";
    private static App instance;


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

    }

    public static App getInstance(){
        return instance;
    }
}
