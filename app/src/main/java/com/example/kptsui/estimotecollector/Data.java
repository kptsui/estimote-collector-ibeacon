package com.example.kptsui.estimotecollector;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.Utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

/**
 * Created by user on 26/10/2016.
 */

/*
Assumption:
Beacons' major id start from 1 to x continuously
 */
public class Data {
    public static int DEPLOY_BEACONS = 0;

    public double x;
    public double y;

    public Beacon[] beacons;

    // should only be invoked in setRangingListener
    public Data(double x, double y, final List<Beacon> receiveBeacons){
        this.x = x;
        this.y = y;

        this.beacons = new Beacon[DEPLOY_BEACONS];
        if(receiveBeacons == null || receiveBeacons.size() == 0){
            for(int i = 0; i < this.beacons.length ; i++){
                this.beacons[i] = new Beacon(null, null, i+1, 0, 0, 0);
            }
        }
        else{
            for(int i = 0; i < receiveBeacons.size() ; i++){
                Beacon receiveBeacon = receiveBeacons.get(i);
                int bid = receiveBeacon.getMajor();

                if(bid <= this.beacons.length){
                    this.beacons[bid - 1] = receiveBeacon;
                }
            }

            Beacon temp = receiveBeacons.get(0);
            for(int i = 0; i < this.beacons.length ; i++){
                if(this.beacons[i] == null)
                    this.beacons[i] = new Beacon(
                            temp.getProximityUUID(),
                            temp.getMacAddress(),
                            i+1,
                            temp.getMinor(),
                            temp.getMeasuredPower(),
                            0);
            }
        }
    }

    public void setAverageBeacons(double[] avg_rssi){
        for(int i = 0; i < this.beacons.length ; i++){
            if(this.beacons[i] != null){
                this.beacons[i] = new Beacon(
                        this.beacons[i].getProximityUUID(),
                        this.beacons[i].getMacAddress(),
                        this.beacons[i].getMajor(),
                        this.beacons[i].getMinor(),
                        this.beacons[i].getMeasuredPower(),
                        (int)avg_rssi[i]
                );
            }
        }

    }

    @Override
    public String toString() {
        StringBuilder row = new StringBuilder();
        row.append(x).append(",").append(y);
        for (Beacon b : this.beacons){
            if(b == null){
                row.append(",").append(0);
            }
            else{
                row.append(",").append(b.getRssi());
            }
        }
        row.append("\n");

        return row.toString();
    }
}
